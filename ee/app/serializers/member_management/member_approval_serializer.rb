# frozen_string_literal: true

module MemberManagement
  class MemberApprovalSerializer < BaseSerializer
    entity MemberApprovalEntity
  end
end
